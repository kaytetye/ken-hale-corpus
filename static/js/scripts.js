
  var getCookie = function(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  }

  var setCookie = function(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }


  var toggleThings = function (cookieName, switchId, elClassName ) {
    console.log("toggleThings", cookieName, switchId, elClassName)

    // get the cookie value, default to false
    var cookieValue = (getCookie(cookieName)=='true')
    // toggle it
    cookieValue = !cookieValue
    // save it
    setCookie(cookieName, cookieValue, 30)
    // update switch ui
    setElementByIdState(switchId, cookieValue)
    // update UI elements
    if (elClassName !== null) {
      setElementsState(elClassName, cookieValue)
    }
  }

  var setElementByIdState = function(id, state) {
    var element = document.getElementById(id)
    if (state==true) element.classList.add('selected')
    else element.classList.remove('selected')
  }

  var setElementsState = function (elClassName, state){
    console.log("setElementsState", elClassName, state)
    var guids = document.querySelectorAll(elClassName)
    guids.forEach(element => {
      if (state==true) element.classList.remove('hide-display')
      else element.classList.add('hide-display')
    })
  }
