---
title: "Alec Kapetye"
date: 2018-01-22T17:16:26+10:00
---

Alec Kapetye was from Kwerrkepentye. He was a stockman who worked at Neutral Junction station and in later life worked with linguist Harold Koch documenting the Kaytetye language. He married Daisy Kemarre, whose story can also be heard on this website.

### Listen to Alec Kapetye

- [KOCH_H03-037143](/stories/akapetye)
