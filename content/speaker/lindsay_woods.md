---
title: "Lindsay Woods Pengarte"
date: 2018-01-22T17:17:04+10:00
---

Lindsay Woods Pengarte grew up at Barrow Creek. His traditional lands are Warlekerlange, specifically, Elkertelhe, a fire Dreaming country west of Barrow Creek. He married a Luritja woman and moved to Amoongana where he passed away. On the recording 4560 Ken Hale can be heard eliciting from Western Arrernte, with Lindsay saying the equivalent Kaytetye utterance. Some of Lindsay’s words are pronounced with a Western Arrernte accent. For example, he says *tererre* instead of *twererre* ‘clapsticks’ and he also sometimes uses Western Arrernte words instead of the Kaytetye, such as *thip* instead of *thangkerne* ‘bird’.

### Listen to Lindsay Woods Pengarte

- [kh4560](/kenhale/kh4560)
