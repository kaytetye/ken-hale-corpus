---
title: "Physic Kemarre"
date: 2018-01-22T17:17:52+10:00
---

This may have been Numiad Bailey Physic, a man born in 1909 of Pwerle skin. He worked as a tracker at Barrow Creek in the 1940s and lived at Hatches Creek in 1954 where his wife, Molly Penangke from Marlakurlangu, worked as a house girl. Their two children, David and Danny, were at school at the Bungalow in 1957. We think Physic was from Wardingikurlangu, Renny Swamp, a place near the Mt Peake region of the Hanson River; and his mother may have been a Napaljarri from Ngarnka, Mt Leichardt.

### Listen to Physic Kemarre

- [kh4565](/stories/kh4565)
