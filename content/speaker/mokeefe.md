---
title: "Molly O’Keefe Kngwarraye"
date: 2018-01-22T17:16:26+10:00
---

Molly O’Keefe Kngwarraye is a Kaytetye woman from Jarra-Jarra, north west of Barrow Creek. She told this story about her life to linguist Jennifer Green for CAAMA on 31 August 1983 at Alekarenge. Some of this story appeared on the Kaytetye Show, hosted by Emily Hayes on CAAMA in 1986. Molly passed away in 2000.

It was transcribed and translated by Myfany Turpin, Shirleen McLaughlin and Emily Hayes in 1999, with assistance from Jacob Peltharr and Hilda Price at Stirling in 2002.

© Intellectual content: Molly O’Keefe Kngwarraye 1983.

Reproduced with permission from Mona Haywood, Ena and Maureen O’Keefe 2002.

© Recording Jennifer Green and CAAMA. Used with Permission.

### Listen to Molly O’Keefe Kngwarraye

- [mokeefe](/stories/mokeefe)
