---
title: ""
date: 2018-01-22T16:58:49+10:00
body_classes: "home"
featured_image: images/arelparenge100314-mt-sm.jpg
---

# Kaytetye recorded in 1959 by Professor Ken Hale

This collection consists of Kaytetye words and sentences as well as a story. The Kaytetye was spoken by six men, many of whom spoke other central Australian languages, including Anmatyerr, Alyawarr, Western Arrernte and Warlpiri. The speakers were probably born some time between 1910–1925. As far as we know, these are the first recordings of the Kaytetye language.

[About the recordings](recordings)
[Acknowledgements](acknowledgements)

| Kaytetye Speaker      | Recorded at              |
| :-------------------- | :----------------------- |
| [Louis Penangke][1]   | Elliott                  |
| [Lindsay Woods][2]    | Barrow Creek             |
| [Peter][3]            | Barrow Creek             |
| [Chablow Peltharr][4] | Murry Downs, Mperlatwaty |
| [Bob Kemarre][5]      | Barrow Creek             |
| [Physic Kemarre][6]   | Barrow Creek             |

# Kaytetye Stories recorded by Emily Hayes for CAAMA

<!--
- [kh4556][4556] | - [Aileen Penangke][apenangke]
- [kh4560][4560] | - [Daisy Kemarre][daisy]
- [kh4561][4561] | - [Tracker Mick Kapetye][trackermick]
- [kh4562][4562] | - [Molly OKeefe][mokeefe]
- [kh4563][4563] |
- [kh4564][4564] |
- [kh4565][4565] |
- [kh4566][4566] |
-->

| Kaytetye Speaker                    | Year recorded |
| :---------------------------------- | :------------ |
| [Molly O’Keefe Kngwarraye][mokeefe] | 1983          |
| [Aileen Penangke][apenangke]        | 1985          |
| [Daisy Kemarre][daisy]              | 1986          |
| [Tracker Mick Kapetye][trackermick] | 1986          |

# Other Kaytetye Stories

| Kaytetye Speaker         | Year recorded |
| :----------------------- | :------------ |
| [Alec Kapetye][akapetye] | 1978          |

[1]: speaker/louis_penangke
[2]: speaker/lindsay_woods
[3]: speaker/peter
[4]: speaker/chablow_peltharr
[5]: speaker/bob_kemarre
[6]: speaker/physic_kemarre
[4556]: kenhale/kh4556
[4560]: kenhale/kh4560
[4561]: kenhale/kh4561
[4562]: kenhale/kh4562
[4563]: kenhale/kh4563
[4564]: kenhale/kh4564
[4565]: stories/kh4565
[4566]: kenhale/kh4566
[apenangke]: speaker/apenangke
[daisy]: speaker/daisy
[trackermick]: speaker/trackermick
[mokeefe]: speaker/mokeefe
[akapetye]: speaker/akapetye

Note: Permission to publish the stories here has been given by descendants of the speakers. While every effort has been made to contact the closest next of kin, this may not have always been achieved. Should you have any queries contact [myfany.turpin@sydney.edu.au](mailto:myfany.turpin@sydney.edu.au).
