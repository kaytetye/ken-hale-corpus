---
title: "Acknowledgements"
date: 2018-01-22T17:28:57+10:00
menu: "main"
---


This website is for people interested in hearing, reading and learning Kaytetye. You can  listen to the whole audio by pressing the play button at the top of the page. For learners, it is useful to see the corresponding text, which you can do by pressing the play button to the left of the text. At the bottom of each audio web page you can turn on 'glossing'. This shows what each part of the sentence means. You can also turn on linguistic glossing, which gives the technical terms for each meaningful element. The names of the speakers and the language spoken is given at the right of the web page.

----

Funding for this project has come from the Australian Research Council through the Centre of Excellence for the Dynamics of Language and the ARC Discovery Project DP150100845. http://www.dynamicsoflanguage.edu.au. We also acknowledge assistance from the Central Land Council in association with ARC Linkage Grant LP140100806.

---

The project is co-ordinated by Myfany Turpin at the University of Sydney. Materials were prepared for online publication (including community consultations) with Alison Ross†, Shirleen McLaughlin and Amy Parncutt; while Ben Foley developed the online platform.  This project builds on many years of previous work on the Kaytetye language by many people. We thank AIATSIS and Ezra Hale for permission to reproduce the Hale collection; as well as and descendants of the known speakers, Amy, Hilda and Lena  Ngamperle; and Billy Bumper. We also thank Nick Thieberger, David Nash, Jane Simpson, Harold Koch and Nay San for providing advice on many aspects of this project.

![Myf Ben Amy](myf_ben_amy2016.png "Amy Ngamperle and Myfany Turpin")
*Amy Ngamperle and Myfany Turpin check the online version of Amy’s aunt’s story.*

![preparing materials in Sydney](sydney2017.png "Ben Foley, Nay San, David Nash, Jane Simpson and Amy Parncutt preparing materials in Sydney 2017")
*Ben Foley, Nay San, David Nash, Jane Simpson and Amy Parncutt preparing materials in Sydney 2017.*

![Shirleen McLaughlin and Gavan Breen](clc_jgi_kaytetyephotos_16_of_103.png "Shirleen McLaughlin and Gavan Breen")
*Shirleen McLaughlin transcribing Kaytetye at IAD ca. 1998. Gavan Breen, who also worked on Kaytetye, in background.*

![Alison Ross and Myfany Turpin](ar_mt_iad_ca2000.png "Alison Ross and Myfany Turpin")
*Alison Ross† and Myfany Turpin working on Kaytetye at IAD in 2000.*

---

<!-- insert Creative Commons license? -->

*We pay our respects to Kaytetye elders, past and present. This website contains traditional knowledge of the Kaytetye people, and have been presented here with the consent of the knowledge custodians. Dealing with any part of the knowledge for any purpose that has not been authorised may breach the customary laws of the Kaytetye people, and may also breach copyright and moral rights under the Copyright Act 1968 (Australian Commonwealth).*

*We thank the following people for assisting us in locating next of kin for the speakers on these recordings:
Shirleen McLaughlin, Christine Palmer, Elaine Williams, Paddy Willis, Tommy Thompson, Alison Ross†, Don Presley, Betsy, Clarrie Kemarr, Aileen Perrwerl, Amy Ngamperl, Carol Thompson, Elsie Numina, Eileen Ampetyane and Jay Gibson.*
