---
title: "Recordings"
date: 2018-01-22T17:28:39+10:00
menu: "main"
---

Ken Hale travelled through the NT recording Aboriginal words, sentences, stories and sometimes songs on a reel to reel tape recorder. Further details of his fieldwork in central Australia can be read in ‘Both sides of the bitumen by Jennifer Green in *Forty years on. Ken Hale and Australian languages,* (2001), edited by J Simpson, D Nash, M Laughren, P Austin & B Alpher.

Many recordings also include a speaker of another language and these languages can also be heard on the recordings, such as Alyawarr (4566) and Mudburra (4560). Ken Hale often repeats the words in these languages, and it can be difficult to distinguish between the different speakers and languages. We use colour coding to help distinguish between the different languages; and initials to distinguish between speakers. Ken Hale also elicited Kaytetye by working with people who also spoke Warlpiri, such as Chablow and Peter. Ken Hale read the Warlpiri—a language that he was more familiar with—and the speakers translated this into Kaytetye. The Warlpiri however was not recorded (e.g. 4563, 4564).

### The language on the recordings

Many of the men on these recordings also spoke Anmatyerr or Alyawarr, languages very similar to Kaytetye and much more widely spoken. Sometimes the men used words from these other languages instead of the Kaytetye word. Multilingualism was very common for that generation and it is probably somewhat artificial, or at least unnatural, to hold a conversation with someone who spoke another language and not use words from the more widely spoken language, such as Alyawarr or Anmatyerr. While most of these non-Kaytetye words are presented here too, we have added a note with the equivalent Kaytetye term.

### Blanked out words

Some words on the recording have been omitted here, along with their translations. We have done this for one of three reasons:

- because they are grammatical errors
- because they are words from another language (usually Anmatyerr or Alyawarr)
- because they are considered rude or inappropriate for public display

### Transcriptions 

Ken Hale wrote down and translated what he heard on the spot using a broad International Phonetic Alphabet.  Ken Hale had an excellent ear and we include his hand written transcriptions here. As can be seen, there are sometimes discrepancies between what Ken Hale wrote and the standard spelling. This is because there are often more than one way to pronounce a word. 

There was no standard spelling system for writing Kaytetye back in 1959. The spelling system used here follows that of Turpin & Ross 2013 https://iadpress.com/shop/kaytetye-to-english-dictionary-available-february-2012-2/. While Ken Hale’s aim was to write down exactly what he heard; a spelling system aims to capture only the meaningful sounds, which means ignoring much of the minor variations in pronunciation that have no relevance to the meaning of a word. 

In 1997 Ken Hale’s notes were typed up by Kathryn Flack, a volunteer at the Institute for Aboriginal Development’s Central Australian Dictionaries Program. In 2016 these were checked by Alison Ross and glossed by Myfany Turpin, Shirleen McLaughlin and Amy Parncutt. The Mudburra was transcribed by David Osgarby.
 
### Translations

Some Kaytetye words have no exact short translation equivalent. This is especially the case with pronouns. We translate the 3rd person pronoun as ‘he /she / it’; and we provide a sample kin term in brackets in translations of plural pronouns, although this should be understood as only one of kin categories that the pronoun can include (see Turpin 2001 https://iadpress.com/shop/learners-guide-to-kaytetye/).
 
### The original recordings

When the recordings were digitised by Australian Institute of Aboriginal Studies (AIATSIS), the AIATSIS archive numbers were read out and recorded onto the digital copies of the recording. We have omitted these announcements here to create a more immediate feel for the context in which they were recorded. The time removed from the original recording is noted in the table below. For those wishing to access the AIATSIS archived recording, this time should be added to the beginning of the files. We have, however, kept the few instances of AIATSIS audio announcements that occur in the middle of the file to simplify the calculation of time codes.

AIATSIS file name  | time    | beginning time | speaker
-------------------|--------:|:--------------:|--------------------
Hale_K06-4556      | 20”     | 20 seconds     | Louis Penangke
Hale_K06-4560      | 1’03”   | 17 seconds     | Lindsay Wood, Peter
Hale_K06-4561      | 1’01”   | 17 seconds     | Peter
Hale_K06-4562      | 03”     | 17 seconds     | Peter
Hale_K06-4563      | 1’02”   | 22 seconds     | Peter, Chablow
Hale_K06-4564      | 47”     | 19 seconds     | Chablow
Hale_K06-4565      | 40”     | 21 seconds     | Bob, Physic
Hale_K06-4566      | 21”     | 19 seconds     | Chablow

Note: Permission to publish the stories here has been given by descendants of the speakers. While every effort has been made to contact the closest next of kin, this may not have always been achieved. Should you have any queries contact [myfany.turpin@sydney.edu.au](mailto:myfany.turpin@sydney.edu.au).   



